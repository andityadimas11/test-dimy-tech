-- Create Order table
CREATE TABLE orders (
    id SERIAL PRIMARY KEY,
    customer_id INT NOT NULL REFERENCES customer(id),
    address_id INT NOT NULL REFERENCES customer_address(id),
    order_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE order_product (
    order_id INT NOT NULL REFERENCES order(id),
    product_id INT NOT NULL REFERENCES product(id),
    PRIMARY KEY (order_id, product_id)
);

CREATE TABLE order_payment_method (
    order_id INT NOT NULL REFERENCES order(id),
    payment_method_id INT NOT NULL REFERENCES payment_method(id),
    PRIMARY KEY (order_id, payment_method_id)
);


-- //optimization

-- Create Order table
CREATE TABLE order (
    id SERIAL PRIMARY KEY,
    customer_id INT NOT NULL REFERENCES customer(id),
    address_id INT NOT NULL REFERENCES customer_address(id),
    order_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- Create OrderProduct table (to handle the many-to-many relationship between Order and Product)
CREATE TABLE order_product (
    order_id INT NOT NULL REFERENCES order(id),
    product_id INT NOT NULL REFERENCES product(id),
    PRIMARY KEY (order_id, product_id)
);

-- Create OrderPaymentMethod table (to handle the many-to-many relationship between Order and Payment Method)
CREATE TABLE order_payment_method (
    order_id INT NOT NULL REFERENCES order(id),
    payment_method_id INT NOT NULL REFERENCES payment_method(id),
    PRIMARY KEY (order_id, payment_method_id)
);
