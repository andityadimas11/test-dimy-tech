package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type Transaction struct {
	CustomerID     int   `json:"customer_id"`
	AddressID      int   `json:"address_id"`
	Products       []int `json:"products"`
	PaymentMethods []int `json:"payment_methods"`
}

var db *pgxpool.Pool

func createTransaction(c echo.Context) error {
	var transaction Transaction
	if err := c.Bind(&transaction); err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{"message": "Invalid request payload"})
	}

	tx, err := db.Begin(c.Request().Context())
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": "Failed to start transaction"})
	}
	defer tx.Rollback(c.Request().Context())

	orderDate := time.Now()

	// Insert transaction into "transaction_order" table
	var orderID int
	err = tx.QueryRow(c.Request().Context(), `
		INSERT INTO transaction_order (customer_id, address_id, order_date)
		VALUES ($1, $2, $3) RETURNING id`, transaction.CustomerID, transaction.AddressID, orderDate).Scan(&orderID)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": "Failed to create transaction"})
	}

	// Insert products into "order_product" table
	for _, productID := range transaction.Products {
		_, err := tx.Exec(c.Request().Context(), `INSERT INTO order_product (order_id, product_id) VALUES ($1, $2)`, orderID, productID)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": "Failed to associate products"})
		}
	}

	// Insert payment methods into "order_payment_method" table
	for _, paymentMethodID := range transaction.PaymentMethods {
		_, err := tx.Exec(c.Request().Context(), `INSERT INTO order_payment_method (order_id, payment_method_id) VALUES ($1, $2)`, orderID, paymentMethodID)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"message": "Failed to associate payment methods"})
		}
	}

	if err := tx.Commit(c.Request().Context()); err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": "Failed to commit transaction"})
	}

	return c.JSON(http.StatusOK, map[string]string{"message": "Transaction created successfully"})
}

func main() {
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Connect to the PostgreSQL database
	connStr := "user=andityadimas dbname=postgres sslmode=disable"
	fmt.Println(connStr, "test koneksi")

	pool, err := pgxpool.Connect(context.Background(), connStr)
	if err != nil {
		fmt.Println("Failed to connect to the database:", err)
		return
	}

	// Ping the database to check the connection
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := pool.Ping(ctx); err != nil {
		fmt.Println("Failed to ping the database:", err)
		return
	}
	fmt.Println("Database connection established")

	// Close the database connection when the program exits
	defer pool.Close()

	e.POST("/transactions", createTransaction)
	e.Logger.Fatal(e.Start(":8080"))
}
